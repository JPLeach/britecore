import Vue from 'vue'
import App from './App'
import Header from './Header.vue'
import Types from './Types.vue'
import Fields from './Fields.vue'
import Details from './Details.vue'
import Tags from './Tags.vue'
import Controls from './Controls.vue'
import Groups from './Groups.vue'
import Inputs from './Inputs.vue'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('icon', Icon)
Vue.component('app-header', Header)
Vue.component('app-types', Types)
Vue.component('app-fields', Fields)
Vue.component('app-details', Details)
Vue.component('app-tags', Tags)
Vue.component('app-controls', Controls)
Vue.component('app-groups', Groups)
Vue.component('app-inputs', Inputs)

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
